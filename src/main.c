#include "mem.h"
#include "mem_internals.h"
#include <assert.h>
#include <stdio.h>


static void test_successful_allocation() {
    void* heap = heap_init(0);
    printf("== Test: Allocation ==\n");
    debug_heap(stdout, heap);

    void* mem = _malloc(0);
    assert(mem != NULL && "Allocation failed");
    printf("Allocation success: %p\n", mem);
    _free(mem);
    heap_term();
    printf("Free success\n\n");
}

static void test_free_single() {
    void* heap = heap_init(0);
    printf("== Test: Free single block ==\n");
    debug_heap(stdout, heap);
    void* mem1 = _malloc(64);
    assert(mem1 != NULL && "Allocation failed for block 1");
    debug_heap(stdout, heap);
    _free(mem1);
    debug_heap(stdout, heap);
    heap_term();
    printf("Single block freed successfully\n\n");
}

static void test_free_multiple() {
    void* heap = heap_init(0);
    printf("== Test: Free multiple blocks ==\n");
    debug_heap(stdout, heap);
    void* mem1 = _malloc(64);
    void* mem2 = _malloc(128);
    assert(mem1 != NULL && "Allocation failed for block 1");
    assert(mem2 != NULL && "Allocation failed for block 2");
    debug_heap(stdout, heap);
    _free(mem1);
    _free(mem2);
    debug_heap(stdout, heap);
    heap_term();
    printf("Multiple blocks freed successfully\n\n");
}

static void test_region_extension() {
    heap_init(0);
    printf("== Test: Region Extension ==\n");
    debug_heap(stdout, HEAP_START);

    void* mem1 = _malloc(4096);
    void* mem2 = _malloc(8129);
    assert(mem1 != NULL && "First allocation failed");
    assert(mem2 != NULL && "Second allocation failed, possibly due to insufficient heap extension");

    debug_heap(stdout, HEAP_START);

    _free(mem1);
    _free(mem2);

    debug_heap(stdout, HEAP_START);
    heap_term();
    printf("Region extension and free successful\n\n");
}


static void test_region_extension_with_obstacle() {
    heap_init(0);
    printf("== Test: Region Extension with Obstacle ==\n");
    debug_heap(stdout, HEAP_START);

    void* obstacle = _malloc(4096);

    void* mem = _malloc(8192);
    assert(obstacle != NULL && "Obstacle allocation failed");
    assert(mem != NULL && "Allocation failed, obstacle may not have been considered");

    debug_heap(stdout, HEAP_START);

    _free(obstacle);
    _free(mem);

    debug_heap(stdout, HEAP_START);
    heap_term();
    printf("Region extension with obstacle and free successful\n\n");
}

int main() {
    test_successful_allocation();
    test_free_single();
    test_free_multiple();
    test_region_extension();
    test_region_extension_with_obstacle();
    return 0;
}